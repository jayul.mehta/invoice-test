package za.co.digitalplatoon.invoiceservice.invoice.interfaces;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import za.co.digitalplatoon.invoiceservice.invoice.application.InvoiceService;
import za.co.digitalplatoon.invoiceservice.invoice.domain.invoice.Invoice;

@RestController
@RequestMapping(value = "/invoice")
public class InvoiceController {

	@Autowired
	InvoiceService invoiceService;

	@ApiOperation(value = "add invoice", response = Boolean.class)
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Boolean addInvoice(@RequestBody @NotNull Invoice invoice) {
		invoiceService.addInvoice(invoice);
		return true;
	}

	@ApiOperation(value = "view all invoice", response = List.class)
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Invoice> viewAllInvoices() {
		return invoiceService.viewAllInvoices();
	}

	@ApiOperation(value = "get by invoiceId", response = Invoice.class)
	@RequestMapping(value = "/get/{invoiceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Invoice viewInvoice(@PathVariable("invoiceId") Long id) {
		return invoiceService.viewInvoice(id).orElse(null);
	}
}
